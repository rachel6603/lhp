﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(BuildBundles))]
public class BuildBundleEditor : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GUILayout.BeginVertical();
        if(GUILayout.Button("Build Android"))
        {
           BuildAllAssetBundles("Android");
        }
        if(GUILayout.Button("Build StandaloneWindows"))
        {
         BuildAllAssetBundles("StandaloneWindows");
        }
        GUILayout.EndVertical();
    }
    public void BuildAllAssetBundles(string name)
    {
        string bundlePath = "Assets/AssetBundles/";

        if (name == "Android")
            BuildPipeline.BuildAssetBundles(bundlePath+"Android/", BuildAssetBundleOptions.UncompressedAssetBundle, BuildTarget.Android);
        else if (name == "StandaloneWindows")
            BuildPipeline.BuildAssetBundles(bundlePath+"StandaloneWindows/", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
    }
}
