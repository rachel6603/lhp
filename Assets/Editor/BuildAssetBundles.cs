﻿using System.IO;
using UnityEditor;
using UnityEngine;

public class BuildAssetBundles : Editor
{

    [MenuItem("Assets/Build AssetBundles Specific")]
    private static void BuildAllAssetBundlesSpecific()
    {
        string bundlePath = "Assets/AssetBundles/watercraft.unity3d";
        Object[] selectedAssets = Selection.GetFiltered(typeof(Object), SelectionMode.Assets);
        BuildPipeline.BuildAssetBundle(Selection.activeObject, selectedAssets, bundlePath, BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.CompleteAssets, BuildTarget.StandaloneWindows);
    }
    [MenuItem("Assets/Build AssetBundles")]
    private static void BuildAllAssetBundles()
    {
        string bundlePath = "Assets/AssetBundles/";

        BuildPipeline.BuildAssetBundles(bundlePath, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
    }
}
